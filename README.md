# text\_kanban.vim

Extremely simple task kanban system with text (or markdown) files.

# Example

1. Load the plugin by some vim plugin manager or just `:source plugin/text_kanban.vim`.
1. Open `sample/sample-board.md` in this repository.
1. Execute `:TextKanbanOpenBoard`.
1. Enjoy!

This project's tasks are also managed with text\_kanban.  
See [tasks/board.md](tasks/board.md).

# Features

- Align text files listed in the board file vertically. You can edit the files as task lists.
    - Tips: Run `vim task-board-file.md -c TextKanbanOpenBoard` to open the task lists immediately.
- :new: Text object for task object:
    - Requires [vim-textobj-user](https://github.com/kana/vim-textobj-user).
    - Yank a task with `yaT`, `yiT`.
    - Delete a task with `daT`, `diT`.

## Useful Keybindings

NOTE: `<Leader>` is `\` by default.

- `<Leader>>>`: Move the task pointed by the cursor (and the cursor itself) to the right task list.
- `<Leader><<`: Move the task pointed by the cursor (and the cursor itself) to the left task list.
- `<Leader>><`: Move the task pointed by the cursor (only the task) to the right task list.
- `<Leader><>`: Move the task pointed by the cursor (only the task) to the left task list.

# Caveat: Conflict with bogado/file-line plugin

When you have [file:line plugin](https://github.com/bogado/file-line) installed, `vim task-board-file.md -c TextKanbanOpenBoard` doesn't work.  
The file:line plugin keeps the kanban file buffer open even after switching to the first list file.

Workaround:

```
vim task-board-file.md --cmd "let g:loaded_file_line = 1" -c TextKanbanOpenBoard
```
