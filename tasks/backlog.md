Document: Screencast for README
Other: Add to vim-textobj-user wiki
    - https://github.com/kana/vim-textobj-user/wiki
Feature: Adjust width of each list
Spec: board file
Spec: task list and task
Spec: task labels
    - Like: this.
    - Deadline: 2999/12/31
Bug: `shift_to/move_right/left` commands dont' work when the default keybindings are changed.
Feature: Strip markdown links
    - For users who see the task board on GitHub, GitLab, etc.
    - Task list names should be able to be written as markdown links.
    - Which enables the users to click the link to quickly see the contents of the task list on GitHub, GitLab etc.
    - Comment: I found I have to insert markdown breaklines to make the list readable on gitlab.com...
Feature: open borad given by command's argument.
Feature: Deadline labels
    - Depends on: task labels
