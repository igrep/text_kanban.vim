This is a sample board file of text\_kanban.vim.  
Lines starting with `- ` are list name lines.  
The list name is interpreted as the file path (relative to the board file) of the list.

- backlog.md
- current.md
- done.md
