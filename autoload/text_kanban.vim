let s:LIST_NAME_REGEXP = '''\v^-\s+'''
let s:IS_LIST_NAME_LINE = 'v:val =~ ' . s:LIST_NAME_REGEXP
let s:EXTRACE_LIST_NAME = 'substitute(v:val, ' . s:LIST_NAME_REGEXP . ', "", "")'
let s:TASK_HEADER_REGEXP = '\v^\S+'
let s:TASK_BODY_REGEXP = '\v^\s+'

""""""""""""""""""""""""""""""""""
" Basic features: open a board
""""""""""""""""""""""""""""""""""

function! s:extract_list_names(lines) abort
  return
        \ reverse(
        \   map(
        \     filter(copy(a:lines), s:IS_LIST_NAME_LINE),
        \     s:EXTRACE_LIST_NAME
        \   )
        \ )
endfunction

function! s:open_list(path) abort
  exec 'vsplit ' . a:path
  " Enables folding by task details
  set foldmethod=indent
endfunction

function! text_kanban#open_board() abort
  let board_buf = bufnr('')
  let board_dir = expand('%:h')

  let list_names = s:extract_list_names(getline(1, '$'))

  let last_list_name = remove(list_names, -1)

  for list_name in list_names
    call s:open_list(board_dir . '/' . list_name)
  endfor

  call s:open_list(board_dir . '/' . last_list_name)
  let last_list_buf = bufnr('')

  exec bufwinnr(board_buf) . ' wincmd w'
  close

  exec bufwinnr(last_list_buf) . ' wincmd w'
endfunction

""""""""""""""""""""""""""""""""""
" Move tasks: internal functions
""""""""""""""""""""""""""""""""""

function! text_kanban#move_right() abort
  normal daT
  wincmd l
  normal! Gp
endfunction

function! text_kanban#shift_to_right() abort
  call text_kanban#move_right()
  wincmd h
endfunction

function! text_kanban#move_left() abort
  normal daT
  wincmd h
  normal! Gp
endfunction

function! text_kanban#shift_to_left() abort
  call text_kanban#move_left()
  wincmd l
endfunction

""""""""""""""""""""""""""""""""""
" Move tasks: user interface
""""""""""""""""""""""""""""""""""

nnoremap <Plug>(textkanban-moveright) :<C-u>call text_kanban#move_right()<CR>
nnoremap <Plug>(textkanban-moveleft) :<C-u>call text_kanban#move_left()<CR>
nnoremap <Plug>(textkanban-shifttoright) :<C-u>call text_kanban#shift_to_right()<CR>
nnoremap <Plug>(textkanban-shifttoleft) :<C-u>call text_kanban#shift_to_left()<CR>

map <Leader>>> <Plug>(textkanban-moveright)
map <Leader><< <Plug>(textkanban-moveleft)
map <Leader>>< <Plug>(textkanban-shifttoright)
map <Leader><> <Plug>(textkanban-shifttoleft)

""""""""""""""""""""""""""""""""""
" Define text object for Task
""""""""""""""""""""""""""""""""""

function! text_kanban#select_task() abort
  return ['V', s:detect_task_start(), s:detect_task_end()]
endfunction

function! s:detect_task_start() abort
  while !(getline('.') =~ s:TASK_HEADER_REGEXP || line('.') == 1)
    normal! k
  endwhile
  return getpos('.')
endfunction

function! s:detect_task_end() abort
  let result = getpos('.')

  if result[1] == line('$')
    return result
  endif

  if getline('.') =~ s:TASK_HEADER_REGEXP
    normal! j
  endif

  while 1
    let result = getpos('.')

    if getline('.') =~ s:TASK_HEADER_REGEXP
      " The end of the task is before the next task's head.
      let result[1] -= 1
      return result
    endif

    if result[1] == line('$')
      return result
    endif

    normal! j
  endwhile
endfunction

try
  " TODO: I'm not sure move-N-function and move-P-function are correctly implemented...
  call textobj#user#plugin('textkanbantask', {
        \   '-': {
        \     'move-n': s:TASK_HEADER_REGEXP,
        \     'move-p': s:TASK_HEADER_REGEXP,
        \     'move-N-function': 'text_kanban#select_task',
        \     'move-P-function': 'text_kanban#select_task',
        \     'select-i-function': 'text_kanban#select_task',
        \     'select-a-function': 'text_kanban#select_task',
        \     'select-a': 'aT',
        \     'select-i': 'iT',
        \   }
        \ })
catch /^E117\>/
  echomsg "text_kanban: WARN: textobj#user#plugin function is not defined. Can't define text object for tasks."
endtry
